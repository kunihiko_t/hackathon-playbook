# Ansible Playbook for Casley Hackathon

Available till 2014/04/05

## Installation

Install Vagrant

http://www.vagrantup.com/

Install VirtualBox

https://www.virtualbox.org/wiki/Downloads

Install homebrew

	ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"
	brew install ansible

Clone this repository

	git@bitbucket.org:kunihiko_t/hackathon-playbook.git

`vagrant up` to get

* rbenv + ruby
* rails
* nvm + Node.js
* Mongodb
* Hackathon starter https://github.com/sahat/hackathon-starter

`vagrant ssh`

Rails

	cd /vagrant/rails/sample
	bundle exec rails server

Node.js

	cd /vagrant/hackathon-starter
	nodemon app.js

Publish apps with `vagrant share`


## Requirements
* VirtualBox 4.3.8 or later
* Vagrant 1.5.0 or later
* Ansible 1.5 or later
